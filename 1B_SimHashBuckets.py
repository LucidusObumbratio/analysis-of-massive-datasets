import numpy as np
import hashlib
import time
from collections import defaultdict

__author__ = "Maciej Dąbrowski"
__copyright__ = "Copyright (C) 2018 Maciej Dąbrowski"
__version__ = "1.0"

"""
LSH algorithm. It is much more efficient than SimHash and is great for bigger amounts of data.

The data file (R.in) has following structure:
N <- number of further text lines
n_text_lines
Q <- number of queries
q_queries

The desired output (R.out) has only answers for queries in every new line.
"""

def get_hash_string(unit):
    hash_hex = hashlib.md5(unit.encode()).hexdigest()
    return '{0:0128b}'.format(int(hash_hex, 16))

def simhash(units):
    """Calculates hash and returns it as array of size 128"""
    a = np.array([np.array([1 if x == '1' else -1 for x in get_hash_string(unit)]) for unit in units])
    return np.array([1 if x >= 0 else 0 for x in np.sum(a, axis=0)])
###


def is_close(s1, s2, k):
    """Calculates the Hamming distance between two bit strings
       if difference is grater than 'k' - function returns 0 otherwise 1"""
    assert len(s1) == len(s2)
    diff_count = 0
    for i in range(len(s1)):
        diff_count += 1 if s1[i] != s2[i] else 0

        if diff_count > k:
            return 0

    return 1
###


def hash2int(band, hash, r):
    hash_slice = hash[band * r : (band + 1) * r]
    return sum([hash_slice[p] * (2 ** (r - (p + 1))) for p in range(r)])
###


def sim_hash_buckets(filepath):

    b = 8  # band number
    hash_size = 128
    r = int(hash_size / b)
    candidates = defaultdict(set)

    file = open(filepath + 'R.in', 'r')
    n = int(file.readline())

    time_start = time.time()
    hash_list = [simhash(file.readline().split()) for _ in range(n)]
    time_hashing = time.time() - time_start
    print('Hashing time: {0}'.format(time_hashing))

    # LSH
    time_start = time.time()
    for band in range(b):
        buckets = defaultdict(set)
        for c_id in range(n):  # c_id - candidate_id
            value = hash2int(band, hash_list[c_id], r)
            txt_in_buckets = set()

            if buckets[value]:
                txt_in_buckets = buckets[value]

                for t_id in txt_in_buckets:
                    candidates[c_id].add(t_id)
                    candidates[t_id].add(c_id)

            txt_in_buckets.add(c_id)
            buckets[value] = txt_in_buckets
    time_banding = time.time() - time_start
    print('LSH time: {0}'.format(time_banding))

    q = int(file.readline())

    time_start = time.time()
    result_list = []
    for q_id in range(q):
        i, k = [int(x) for x in (file.readline()).split()]
        s = sum(is_close(hash_list[i], hash_list[c], k) for c in candidates[i])
        result_list.append(s)
    time_query = time.time() - time_start
    print('Query time: {0}'.format(time_query))

    file.close()

    print('Total time: {0}'.format(sum([time_hashing, time_banding, time_query])))

    file = open(filepath + 'R.out', 'r')

    diff_count = sum(1 if int(file.readline()) != result_list[l_id] else 0 for l_id in range(q))

    print('Diff count: {0}'.format(diff_count))


if __name__ == '__main__':
    sim_hash_buckets('./1B_data/test0/')
