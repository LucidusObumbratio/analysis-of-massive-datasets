import numpy as np
import hashlib
import time

__author__ = "Maciej Dąbrowski"
__copyright__ = "Copyright (C) 2018 Maciej Dąbrowski"
__version__ = "1.0"

"""
SimHash algorithm.

The data file (R.in) has following structure:
N <- number of further text lines
n_text_lines
Q <- number of queries
q_queries

The desired output (R.out) has only answers for queries in every new line.
"""

def get_hash_string(unit):
    hash_hex = hashlib.md5(unit.encode()).hexdigest()
    return '{0:0128b}'.format(int(hash_hex, 16))

def simhash(units):
    """Calculates hash and returns it as array of size 128"""
    a = np.array([np.array([1 if x == '1' else -1 for x in get_hash_string(unit)]) for unit in units])
    return np.array([1 if x >= 0 else 0 for x in np.sum(a, axis=0)])
###


def is_close(s1, s2, k):
    """Calculates the Hamming distance between two bit strings
       if difference is grater than 'k' - function returns 0 otherwise 1"""
    assert len(s1) == len(s2)
    diff_count = 0
    for i in range(len(s1)):
        diff_count += 1 if s1[i] != s2[i] else 0

        if diff_count > k:
            return 0

    return 1
###


def main(filepath):
    file = open(filepath + 'R.in', 'r')

    n = int(file.readline())


    time_start = time.time()
    hash_list = [simhash(file.readline().split()) for _ in range(n)]
    """hash_list = []
    for line_id in range(n):
        words = (file.readline()).split()
        hash_list.append(simhash(words))
    print(len(hash_list), n)"""
    time_end = time.time()

    time_conversion = time_end - time_start

    print('Conversion time: {0}'.format(time_conversion))

    q = int(file.readline())

    time_start = time.time()
    result_list = []
    for query_id in range(q):
        l_id, k = (int(x) for x in (file.readline()).split())
        match_count = sum([is_close(hash_list[l_id], hash_list[line_id], k)
                           if line_id != l_id else 0 for line_id in range(n)])
        result_list.append(match_count)
    time_end = time.time()

    time_query = time_end - time_start
    file.close()

    print('Query time: {0}'.format(time_query))

    file = open(filepath + 'R.out', 'r')
    fail_count = sum(1 if result_list[l_id] != int(file.readline()) else 0 for l_id in range(n))
    file.close()

    print('Total time: {0}'.format(time_query + time_conversion))
    print('Fail count: {0}'.format(fail_count))

if __name__ == '__main__':
    main('./1A_data/')
